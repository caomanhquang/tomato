
// var fileData = "aaa.json";
// var mapCenter = {lat: 30.0, lng: 10.0};
// var mapZoom = 3;
// var spacing = 2.5;
// var zs = [-0.1, 20.0, 50.0, 75.0, 90.0, 95.0, 98.0, 99.0, 99.9, 100.1];
// var colours = ['#000099','#0000FF','#3399FF','#00CCFF','#00CC00','#66FF00','#FFFF00','#CC0000','#FF6633'];

var fileData = "bbb.json";
var mapCenter = {lat: 21.0002005, lng: 105.781918};
var mapZoom = 10;
var spacing = 0.08;
var zs = [-1, 50, 100, 150, 200, 300, 1000];
var colours = ['#00ff00','#ffff00','#ff7e00','#ff0000','#8f3f97','#7e0023'];

var spacingLat = [];
var spacingLng = [];


// Create the Google Map…
var map = new google.maps.Map(d3.select("#map").node(), {
		zoom : mapZoom,
		center : mapCenter,
		mapTypeId : google.maps.MapTypeId.TERRAIN
	});

// Load the data. When the data comes back, create an overlay.
d3.json(fileData, function (data) {
	var overlay = new google.maps.OverlayView();
	
	// Add the container when the overlay is added to the map.
	overlay.onAdd = function () {
	
		// Add the section that will contain the markers of the data points
		var layer = d3.select(this.getPanes().overlayLayer).append("div")
			.attr("class", "stations");
		
		// Add the section that will contain the contour plot
		var cont_layer = d3.select(this.getPanes().overlayLayer).append("div")
				.attr("class","contour").append("svg:svg");
		
		// Add a group to the SVG object; groups can be affected by the opacity
		var cont_group = cont_layer.append("g").attr("opacity",0.3);
		
		// Implement the overlay.draw method
		overlay.draw = function () {
			var projection = this.getProjection(),
			padding = 10;
			
			// function for transforming the data marker
			function transform(d) {
				d = new google.maps.LatLng(d.value[0], d.value[1]);
				d = projection.fromLatLngToDivPixel(d);
				return d3.select(this)
				.style("left", (d.x - padding) + "px")
				.style("top", (d.y - padding) + "px");
			}
			
			//___Add the data markers
			
			// Draw each marker as a separate SVG element as in Mike Bostock's example for d3 and Google Maps
			var marker = layer.selectAll("svg")
				.data(d3.entries(data))
				.each(transform) // update existing markers
				.enter().append("svg:svg")
				.each(transform)
				.attr("class", "marker");
			
			// Add a circle.
			marker.append("svg:circle")
				.attr("r", 3.5)
				.attr("cx", padding)
				.attr("cy", padding);
			
			// Add a label.
			marker.append("svg:text")
				.attr("x", padding - 6)
				.attr("y", padding + 6)
				.attr("dy", ".31em")
				.text(function (d) {
					return (d.value[2].toFixed(2));//"("+d.value[0].toString()+", "+d.value[1].toString()+"): "+
				});
			
			
			//___Add the contour plot
			
			// The data is provided as an array of [lat, lon, value] arrays and it need to be mapped to a grid.
			// Determine the min and max latitudes and longitudes
			var maxY = data[0][0];
			var minY = data[0][0];
			var maxX = data[0][1];
			var minX = data[0][1];
			var spacingX = spacing;
			var spacingY = spacing;
			data.forEach(function(val){
				maxX=maxX>val[1]?maxX:val[1];
				minX=minX<val[1]?minX:val[1];
				maxY=maxY>val[0]?maxY:val[0];
				minY=minY<val[0]?minY:val[0];
			});
			// Create a properly dimensioned array
			let nx = Math.ceil((maxX-minX)/spacingX+1);
			let ny = Math.ceil((maxY-minY)/spacingY+1);
			//console.log(`X: (${minX} - ${maxX}), Y: (${minY} - ${maxY}), nx: ${nx}, ny: ${ny}`);

			var grid=new Array(nx);
			for (var i=0;i<grid.length;i++) {
				grid[i] = Array(ny);
			}
			
			// Fill the grid with the values from the data array
			data.forEach(function(val){
				let i1 = Math.ceil((val[1]-minX)/spacingX);
				let i2 = Math.ceil((val[0]-minY)/spacingY);
				grid[i1][i2]=val[2] || 0;
			});

			for (let i = 0; i < grid.length; i++) {
				for (let j = 0; j < grid[i].length; j++) {
					if (grid[i][j] == null) {
						grid[i][j] = 0;
					}
				}
			}
			
			//Add a "cliff edge" to force contour lines to close along the border.			
			var cliff = -100;
			grid.push(d3.range(grid[0].length).map(function() { return cliff; }));
			grid.unshift(d3.range(grid[0].length).map(function() { return cliff; }));
			grid.forEach(function(nd) {
			  nd.push(cliff);
			  nd.unshift(cliff);
			});
			
			// determine the size of the SVG
			var c2 = projection.fromLatLngToDivPixel(new google.maps.LatLng(minY, maxX));
			var c1 = projection.fromLatLngToDivPixel(new google.maps.LatLng(maxY, minX));
			
			// var svgHeight = 8000; // c2.y - c1.y;  
			// var svgWidth = 8000;  // c2.x - c1.x;
			// var padX = -4000;
			// var padY = -4000;
			var svgHeight = (c2.y - c1.y)*2;  
			var svgWidth = (c2.x - c1.x)*2;
			var padX = -svgHeight/2;
			var padY = -svgWidth/2;
			
			// console.log(svgHeight,svgWidth);
			
			// set the size of the SVG contour layer
			cont_layer
				.attr("width",svgWidth)
				.attr("height",svgHeight)
				.style("position","absolute")
				.style("top",padX)
				.style("left",padY);
			
			// conrec.js requires two arrays that represent the row and column coordinates. 
			// In this case these are an array of latitudes and one of longitudes
			var latpy = new Array();
			var lonpx = new Array();
			
			// Adding the cliff implies extending the latitude and longitude arrays beyound the minimum and maximum
			for (var i = 0; i < grid[0].length; i++)
				latpy[i] = minY + spacingY * (i-1);
			
			for (var i = grid.length-1; i>=0; i--)
				lonpx[i] = minX + spacingX * (i-1);
			
			// define the colours to be used and the corresponding limits
			//var colours = ['#000099','#0000FF','#3399FF','#00CCFF','#00CC00','#66FF00','#FFFF00','#CC0000','#FF6633'];
				//zs = [-0.1, 20.0, 50.0, 75.0, 90.0, 95.0, 98.0, 99.0, 99.9, 100.1];
			
			// create a Conrec object and compute the contour
			var c = new Conrec();
			// console.log('XXX grid: ', JSON.stringify(grid));
			// console.log('lonpx: ', lonpx);
			// console.log('latpy: ', latpy);
			c.contour(grid, 0, lonpx.length-1, 0, latpy.length-1, lonpx, latpy, zs.length, zs);
			
			// draw the contour plot following Jason Davies example for conrec.js and d3
			var cont = cont_group.selectAll("path").data(c.contourList())
				// update existing paths
				.style("fill",function(d) { 
					return colours[zs.indexOf(d.level)-1];
				})
				.style("stroke","black")
				.attr("d",d3.svg.line()
					// the paths are given in lat and long coordinates that need to be changed into pixel coordinates
					.x(function(d) { return (projection.fromLatLngToDivPixel(new google.maps.LatLng(d.y, d.x))).x - padX; })
					.y(function(d) { return (projection.fromLatLngToDivPixel(new google.maps.LatLng(d.y, d.x))).y - padY; })
					)
				.enter().append("svg:path")
				.style("fill",function(d) { 
				return colours[zs.indexOf(d.level)-1];
				})
				.style("stroke","black")
				.attr("d",d3.svg.line()
					// the paths are given in lat and long coordinates that need to be changed into pixel coordinates
					.x(function(d) { return (projection.fromLatLngToDivPixel(new google.maps.LatLng(d.y, d.x))).x - padX; })
					.y(function(d) { return (projection.fromLatLngToDivPixel(new google.maps.LatLng(d.y, d.x))).y - padY; })
				);
		}; // overlay.draw
	}; // overlay.onAdd
	
	// Bind our overlay to the map…
	overlay.setMap(map);
});